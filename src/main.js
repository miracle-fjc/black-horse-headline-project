import Vue from 'vue'
import App from './App.vue'

//引入重置样式文件
import '@/style/reset.less'

Vue.config.productionTip = false
console.log(123);

new Vue({
  render: h => h(App),
}).$mount('#app')
